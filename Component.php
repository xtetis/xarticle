<?php

namespace xtetis\xarticle;

class Component extends \xtetis\xengine\models\Component
{

    /**
     * Список действий модуля (они же списки страниц)
     *
     * @var array
     */
    public $module_actions = [
        //================ Формы ==============

        // Форма добавления статьи
        'add_article'               => [
            'method' => 'renderAddArticleForm',
        ],

        //================ Валидация ==============

        // Валидирует форму изменения пароля для неавторизированных пользователей по токену
        'validate_add_article' => [
            'method' => 'validateAddArticleForm',
        ],
        

    ];

    /**
     * Имя модуля в роутах (прописан в composer.json как autoload/psr-4 )
     *
     * @var string
     */
    public $module_name_in_routes = 'xtetis\xarticle';

    /**
     * Рендер формы добавления статьи
     */
    public static function renderAddArticleForm()
    {
        $form = \xtetis\xarticle\controllers\ArticleController::renderAddArticleForm();
        echo \xtetis\xengine\helpers\RenderHelper::renderLayout(
            $form
        );
    }


    /**
     * Валидация формы добавления статьи
     */
    public static function validateAddArticleForm()
    {
        $response = \xtetis\xarticle\controllers\ArticleController::validateAddArticleForm();
        echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    }


    /**
     * Возвращает список статей согласно параметрам
     */
    public static function getArticleList($params = [])
    {
        return \xtetis\xarticle\controllers\ArticleController::getArticleList($params,false);
    }


    /**
     * Возвращает количество статей согласно параметрам
     */
    public static function getArticleListCount($params = [])
    {
        return \xtetis\xarticle\controllers\ArticleController::getArticleList($params,true);
    }

}
