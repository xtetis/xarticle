<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Получаем список дочерних категорий как ассоциативный массив
     * [
     *      ....
     *      id => name,
     *      ....
     * ]
     */
    public static function getCategories(
        $id = 0
    )
    {
        $id      = intval($id);
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql     = '
        SELECT
            c.id
        FROM
            xarticle_category c
        WHERE
            COALESCE(c.id_parent,0)  = COALESCE(:id,0);
        ';
        $stmt = $connect->prepare($sql);

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        $ret  = [];
        foreach ($rows as $row)
        {
            $row['id']       = intval($row['id']);
            $ret[$row['id']] = $row['id'];
        }

        return $ret;
    }

    /**
     * Сохраняем статью
     */
    public static function saveArticle(
        $id_article = 0,
        $name = '',
        $about = '',
        $introtext = ''
    )
    {
        $id_article = intval($id_article);
        $name       = strval($name);
        $about      = strval($about);
        $introtext  = strval($introtext);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xarticle_save_article(:id_article,:name,:about,:introtext,@result,@result_str)');

        $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('name', $name, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('about', $about, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT);
        $stmt->bindParam('introtext', $introtext, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Выполняем динамический SQL для списка статей
     */
    public static function dynamicSqlArticle(
        $sql = '',
        $fetch_one = true
    )
    {

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare($sql);
        $stmt->execute();
        if (!$fetch_one)
        {
            $array = $stmt->fetchAll();
        }
        else
        {
            $array = $stmt->fetch();
        }

        return $array;
    }

    /**
     * Возвращает статью по ID
     */
    public static function getArticleById(
        $id_article = 0
    )
    {
        $id_article = intval($id_article);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT * FROM xarticle_article WHERE id = :id_article');

        $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }

    /**
     * Возвращает ID компонента для указанной категории статей
     */
    /*
    public static function getAnyfieldComponentByCategory(
        $id_category = 0
    )
    {
        $id_category = intval($id_category);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xarticle_get_anyfield_component_by_category(:id_category,@result,@result_str)');

        $stmt->bindParam('id_category', $id_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }
    */

    /**
     * Возвращает список категорий указанной статьи
     */
    public static function getArticleCategories(
        $id_article = 0
    )
    {

        $id_article = intval($id_article);

        $sql = '
        SELECT id_category
        FROM xarticle_article_category t
        WHERE
            t.id_article = :id_article
        ';

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);

        $stmt = $connect->prepare($sql);
        $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();
        $array = $stmt->fetchAll();

        return $array;
    }

    /**
     * Устанавливает категории для статьи
     */
    public static function setArticleInCategories(
        $id_article = 0,
        $id_category_list = []
    )
    {

        $id_article = intval($id_article);

        if (!is_array($id_category_list))
        {
            $id_category_list = [];
        }
        foreach ($id_category_list as &$id_category)
        {
            $id_category = intval($id_category);
        }

        $id_category_list_str = implode(',',$id_category_list);

        $sql = '
        DELETE
        FROM xarticle_article_category
        WHERE
            id_article = :id_article
            AND
            id_category NOT IN ('.$id_category_list_str.')
        ';

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);

        $stmt = $connect->prepare($sql);
        $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        foreach ($id_category_list as &$id_category)
        {
            $sql = '
            SELECT count(*) count FROM xarticle_article_category
            WHERE 
                id_article = :id_article
                AND
                id_category =  :id_category
            ';

            $stmt = $connect->prepare($sql);
            $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
            $stmt->bindParam('id_category', $id_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

            $stmt->execute();
            $array = $stmt->fetch();

            if ($array['count'] <=0)
            {
                $sql = '
                INSERT IGNORE INTO xarticle_article_category
                    (id_article, id_category)
                VALUES
                    (:id_article, :id_category);
                ';
    
                $stmt = $connect->prepare($sql);
                $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
                $stmt->bindParam('id_category', $id_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
    
                $stmt->execute();
            }
        }


        foreach ($id_category_list as &$id_category)
        {
            $sql = '

            DELETE FROM `xarticle_article_category` WHERE `id` IN 
            (
                SELECT 
                    * 
                FROM
                    (
                        SELECT `id` 
                        FROM 
                            `xarticle_article_category` tmp 
                        WHERE 
                            `id_article` = :id_article
                            AND 
                            `id_category` = :id_category
                        ORDER BY 
                            id ASC 
                        LIMIT 
                            999
                        OFFSET 1
                    ) tmp1
            )
            ';

            $stmt = $connect->prepare($sql);
            $stmt->bindParam('id_article', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
            $stmt->bindParam('id_category', $id_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
            $stmt->execute();
        }

    }

    /**
     * Добавляет категорию статьи
     */
    public static function addCategory(
        $id_parent = 0,
        $name = ''
    )
    {

        $id_parent = intval($id_parent);
        $name      = strval($name);

        if (!$id_parent)
        {
            $id_parent = null;
        }

        $sql = '
        INSERT INTO xarticle_category
        (id_parent, name)
        VALUES(:id_parent, :name);';

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);

        $stmt = $connect->prepare($sql);
        $stmt->bindParam('id_parent', $id_parent, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('name', $name, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        $sql  = 'SELECT LAST_INSERT_ID() as id';
        $stmt = $connect->prepare($sql);

        $stmt->execute();
        $array = $stmt->fetch();

        return $array;
    }

    /**
     * Редактирует категорию статьи
     */
    public static function editCategory(
        $id = 0,
        $id_parent = 0,
        $name = ''
    )
    {

        $id        = intval($id);
        $id_parent = intval($id_parent);
        $name      = strval($name);

        if (!$id_parent)
        {
            $id_parent = null;
        }

        $sql = '
        UPDATE xarticle_category
        SET id_parent=:id_parent, name=:name
        WHERE id=:id;';

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);

        $stmt = $connect->prepare($sql);
        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id_parent', $id_parent, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('name', $name, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        return true;
    }

    /**
     * Возвращает компонент (ы), привязанный к указанной каегории статей
     */
    public static function getArticleCategoryXanyfieldComponent(
        $id_article_category = 0
    )
    {

        $id_article_category = intval($id_article_category);

        $sql = '
            SELECT
                t.*
            FROM xarticle_anyfield_component t
            WHERE
                t.id_category = :id_category
        ';

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);

        $stmt = $connect->prepare($sql);
        $stmt->bindParam('id_category', $id_article_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();
        $array = $stmt->fetchAll();

        return $array;
    }

    /**
     * Устанавливает для статьи признак deleted
     */
    public static function setArticleDeleted(
        $id_article = 0,
        $deleted = 0
    )
    {

        $id_article = intval($id_article);
        $deleted    = intval($deleted);
        if (0 != $deleted)
        {
            $deleted = 1;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('UPDATE xarticle_article SET deleted = :deleted WHERE id = :id');

        $stmt->bindParam('deleted', $deleted, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        return true;
    }

    /**
     * Удаляем статью из БД
     */
    public static function deleteArticle(
        $id_article = 0
    )
    {

        $id_article = intval($id_article);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('DELETE FROM xarticle_article WHERE id = :id');

        $stmt->bindParam('id', $id_article, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        return true;
    }

    /**
     * Удаляем категорию статей
     */
    public static function deleteArticleCategory(
        $id_category = 0
    )
    {

        $id_category = intval($id_category);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('DELETE FROM xarticle_category WHERE id = :id');

        $stmt->bindParam('id', $id_category, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);

        $stmt->execute();

        return true;
    }

    /**
     * Возвращает записи из xarticle_anyfield_component
     */
    public static function getArticleAnyfieldComponentList(
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT id FROM xarticle_anyfield_component');
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }

}
