<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ArticleListModel extends \xtetis\xengine\models\Model
{

    /**
     * @var array
     */
    public $additional_where = [];

    /**
     * Из какой категории брать статьи
     */
    public $id_category_list = [];

    /**
     * Количество статей по указанным параметрам
     */
    public $count_articles = 0;

    /**
     * Искать статьи в корзине/удаленные (по-умолчанию не ищем)
     */
    public $deleted = 0;

    /**
     * Искать статьи и в подкатегориях (1 уровень вложенности)
     */
    public $subcategories = 1;

    /**
     * Искать статьи и в подкатегориях (все уровни вложенности)
     */
    public $subcategories_recursive = 1;

    /**
     * Смещение
     */
    public $offset = 0;

    /**
     * Лимит на количество статей (0 = безлимитно)
     */
    public $limit = 10;

    /**
     * Список категорий, из которых выбирать
     */
    public $categories_where_list = [];

    /**
     * Список ID статей
     */
    public $article_id_list = [];

    /**
     * Список моделей статей
     */
    public $article_model_list = [];

    /**
     * @var array
     */
    public $dynamic_sql_article_result = [];

    /**
     * Возвращает список статей согласно параметрам
     * или количество статей согласно параметрам
     */
    public function getArticleIdList()
    {
        $sql = $this->getSql(false);

        $this->dynamic_sql_article_result = \xtetis\xarticle\models\SqlModel::dynamicSqlArticle(
            $sql, 0
        );

        $this->article_id_list = [];
        foreach ($this->dynamic_sql_article_result as $ritem)
        {
            $this->article_id_list[] = $ritem['id'];
        }

        return $this->article_id_list;
    }

    /**
     * Возвращает SQL для генерации
     */
    public function getSql($count = true)
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->deleted                 = intval($this->deleted);
        $this->subcategories           = intval($this->subcategories);
        $this->subcategories_recursive = intval($this->subcategories_recursive);
        $this->limit                   = intval($this->limit);
        $this->offset                  = intval($this->offset);

        if (!is_array($this->additional_where))
        {
            $this->additional_where = [];
        }



        if (!is_array($this->id_category_list))
        {
            $this->id_category_list = [];
        }

        $tmp_article_category_list = $this->id_category_list;
        $this->id_category_list    = [];
        foreach ($tmp_article_category_list as $id_category)
        {
            $this->id_category_list[] = intval($id_category);
        }

        $this->subcategories = intval($this->subcategories);

        if ($count)
        {
            $field_list_return = ' COUNT(*) as count ';
        }
        else
        {
            $field_list_return = ' a.id ';
        }

        $where         = [];
        $where['true'] = ' true ';

        foreach ($this->additional_where as $k => &$v)
        {
            $v = strval($v);
            $where[] = $v;
        }

        // -----------------------------------------------------------
        if ($this->id_category_list)
        {
            $categories_list = $this->id_category_list;

            if ($this->subcategories)
            {

                foreach ($this->id_category_list as $id_child_category)
                {
                    $article_category_model = new \xtetis\xarticle\models\ArticleCategoryModel(
                        [
                            'id' => $id_child_category,
                        ]
                    );

                    $article_category_model->getChildCategories();

                    foreach (array_keys($article_category_model->subcategories_recursive) as $item)
                    {
                        $categories_list[] = $item;
                    }
                }
            }

            $categories_list = array_unique($categories_list);

            $where['category'] = ' a.id IN
                (
                    SELECT ac.id_article
                    FROM
                        xarticle_article_category ac
                    WHERE ac.id_category IN ( ' . implode(',', $categories_list) . ')
                )
            ';
        }
        // -----------------------------------------------------------

        $limit_str = '';
        if ($this->limit)
        {
            $limit_str = ' LIMIT ' . $this->limit . ' ';
        }

        $offset_str = '';
        if ($this->offset)
        {
            $offset_str = ' OFFSET ' . $this->offset . ' ';
        }

        // Если не установлено - показывать и удаленные
        if (!$this->deleted)
        {
            $where['deleted'] = ' a.deleted = 0 ';
        }

        $sql = '
        SELECT ' . $field_list_return . '
        FROM xarticle_article a
        WHERE
        ' . implode(' and ', $where);

        if (!$count)
        {
            $sql .= ' ORDER BY a.id DESC ' .
                $limit_str . $offset_str;
        }

        return $sql;
    }

    /**
     * Определяет количество статей, которые удовлетворяют запросу
     */
    public function getArticleCount()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $sql = $this->getSql(true);

        $this->dynamic_sql_article_result = \xtetis\xarticle\models\SqlModel::dynamicSqlArticle(
            $sql, 1
        );

        $this->count_articles = $this->dynamic_sql_article_result['count'];

        return $this->count_articles;
    }

    /**
     * Возвращает список моделей статей  согласно параметрам
     */
    public function getModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getArticleCount();
        $this->getArticleIdList();

        if ($this->getErrors())
        {
            return false;
        }

        $this->article_model_list = [];

        foreach ($this->article_id_list as $id_article)
        {

            $this->article_model_list[$id_article] = \xtetis\xarticle\models\ArticleModel::generateModelById($id_article);
        }

        return $this->article_model_list;
    }

}
