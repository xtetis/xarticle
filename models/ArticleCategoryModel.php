<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 * Модель категории статей
 */
class ArticleCategoryModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID категории статьи
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xarticle_category';

    /**
     * Список правил для связанных классов
     */
    public $related_rule_list = [
        'id_parent'           => self::class,
    ];

    /**
     * ID родительской категории
     */
    public $id_parent = 0;

    /**
     * Имя категории
     */
    public $name = '';

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Модель родительской категории
     */
    public $parent_category_model = false;

    /**
     * Список дочерних категорий (1-го уровня)
     * в виде ассоциативного массива
     * [
     *  ...
     *  id => name,
     *  ...
     * ]
     */
    public $subcategories = [];

    /**
     * Список всех дочерних категорий (рекурсивно)
     * в виде ассоциативного массива
     * [
     *  ...
     *  id => name,
     *  ...
     * ]
     */
    public $subcategories_recursive = [];

    /**
     * Список дочерних категорий (1-го уровня)
     * в виде массива дерева
     * [
     *      ...
     *      [
     *          'id'=>id_category,
     *          'name' => name_category,
     *      ],
     *      ....
     * ]
     */
    public $subcategories_tree = [];

    /**
     * Список всех дочерних категорий (рекурсивно)
     * в виде массива дерева
     * [
     *      ...
     *      [
     *          'id'=>id_category,
     *          'name' => name_category,
     *          'child'=>[
     *              ...
     *          ],
     *      ],
     *      ....
     * ]
     */
    public $categories_recursive_tree = [];

    /**
     * Привязанные компоненты xanyfield_component
     */
    public $anyfield_component_list = [];

    /**
     * Список моделей компонентов xanyfield, которые привязаны к этой категории
     */
    public $model_anyfield_component_list = [];

    /**
     * Массив с дочерними моделями категорий статей
     */
    public $child_model_list = [];

    /**
     * Список ВСЕХ дочерних моделей галерей (рекурсивно)
     */
    public $all_child_model_list = [];

    /**
     * Список дочерних категорий в виде HTML ul > li ... > ul > li
     */
    public $all_child_list_html = '';



    /**
     * Возвращает информацию о категории
     */
    public function getById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает масств моделей \xtetis\xanyfield\models\ComponentModel 
     * для текущей категории 
     */
    /*
    public function getAnyfieldComponentModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $sql = "
            SELECT
                t.id
            FROM xarticle_anyfield_component t
            WHERE
                t.id_category = :id_category
        ";
        $params = [
            'id_category'=>$this->id,
        ];
        
        return \xtetis\xanyfield\models\ComponentModel::getModelListBySql($sql,$params);
        
    }
    */

    /**
     * Возвращает массив моделей
     * \xtetis\xanyfield\models\ComponentModel для текущей и всех родительских категорий
     */
    public function getModelAnyfieldComponentForAllParentsList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->model_anyfield_component_list)
        {
            return $this->model_anyfield_component_list;
        }

        $sql = "
            SELECT 
                ac.id_anyfield_component id
            FROM 
                xarticle_anyfield_component ac
            WHERE 
                ac.id_category IN (
                    
                    WITH RECURSIVE generation AS (
                        SELECT 
                            id,
                            id_parent
                        FROM xarticle_category
                        WHERE id = :id
                    UNION ALL
                    
                        SELECT 
                            prnt.id,
                            prnt.id_parent
                        FROM xarticle_category prnt
                        JOIN generation g
                        ON g.id_parent = prnt.id
                    )
                    
                    SELECT 
                        id
                    FROM generation	
                
                )
        ";
        $params = [
            'id'=>$this->id,
        ];
        
        $this->model_anyfield_component_list = \xtetis\xanyfield\models\ComponentModel::getModelListBySql($sql,$params);
        return $this->model_anyfield_component_list;
    }

    /**
     * Возвращает список всех дочерних категорий
     * [
     *      ...
     *      id_category => name_category
     *      ....
     * ]
     */
    public function getChildCategories()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Получаем список дочерних категорий как ассоциативный массив
        $this->result_sql['getCategories'] = \xtetis\xarticle\models\SqlModel::getCategories(
            $this->id
        );


        foreach ($this->result_sql['getCategories'] as $key => $value)
        {
            $this->subcategories[$key]           = $value;
            $this->subcategories_recursive[$key] = $value;

            $model_article_category_children = new self([
                'id' => $key,
            ]);
            $model_article_category_children->getChildCategories();
            $this->subcategories_recursive =  $this->subcategories_recursive+  $model_article_category_children->subcategories_recursive;
            /*
            foreach ($model_article_category_children->subcategories_recursive as $inner_key => $inner_value)
            {
                $this->subcategories_recursive[$inner_key] = $inner_value;
            }
            */
        }

        return true;
    }

    /**
     * Возвращает список всех категорий c дочерними категориями
     * [
     *      ...
     *      [
     *          'id'=>id_category,
     *          'name' => name_category,
     *          'child'=>[
     *              ...
     *          ],
     *      ],
     *      ....
     * ]
     */
    public function getChildCategoriesTree()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Получаем список дочерних категорий как ассоциативный массив
        $this->result_sql['getCategories'] = \xtetis\xarticle\models\SqlModel::getCategories($this->id);

        foreach ($this->result_sql['getCategories'] as $key => $value)
        {
            $item_cat                 = [];
            $item_cat['id']           = $key;
            $item_cat['name']         = $value;
            $this->subcategories_tree = $item_cat;

            $model_article_category_children = new self([
                'id' => $key,
            ]);
            $model_article_category_children->getChildCategoriesTree();
            $item_cat['child'] = $model_article_category_children->categories_recursive_tree;

            $this->categories_recursive_tree[] = $item_cat;
        }

        return true;
    }


    /**
     * Возвращает список категорий статей в виде списка ul li
     */
    public function getListHtml(
        $exclude_id = 0,
        $include_id = false
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getChildModelList(true);

        if ($this->child_model_list)
        {
            //echo "gallery_id=".$this->id."\n";
            $this->all_child_list_html = '<ul>';
            foreach ($this->child_model_list as $id_child => $model_article_category)
            {
                //echo 'child_gallery_id=' . $this->id . '->' . $id_child . "\n";
                if ($id_child != $exclude_id)
                {
                    $this->all_child_list_html .= '<li name="' . htmlspecialchars($model_article_category->name) . '" idx="' . $model_article_category->id . '">' . $model_article_category->name;
                    if ($include_id)
                    {
                        $this->all_child_list_html .= ' (' . $model_article_category->id . ')';
                    }

                    $this->all_child_list_html .= $model_article_category->getListHtml($exclude_id, $include_id);
                    $this->all_child_list_html .= '</li>';

                }

            }
            $this->all_child_list_html .= '</ul>';
        }

        return $this->all_child_list_html;
    }

    /**
     * Возвращает список дочерних моделей
     */
    public function getChildModelList(
        $recursive = true
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$recursive)
        {
            if ($this->child_model_list)
            {
                return $this->child_model_list;
            }
        }
        else
        {
            if ($this->all_child_model_list)
            {
                return $this->all_child_model_list;
            }
        }
        

        $sql = "
            SELECT 
                c.id
            FROM 
                xarticle_category c
            WHERE 
                COALESCE(c.id_parent,0)  = COALESCE(:id_category_parent,0);
        ";
        $params = [
            'id_category_parent'=>$this->id
        ];
        // Получаем список дочерних моделей
        $this->child_model_list = self::getModelListBySql($sql,$params);

        if (!$recursive)
        {
            return $this->child_model_list;
        }
        else
        {
            $this->all_child_model_list = $this->child_model_list;
            foreach ($this->child_model_list as $id => $model_article_category)
            {
                $model_article_category->getChildModelList($recursive);
                $this->all_child_model_list + $model_article_category->all_child_model_list;
            }

            return $this->all_child_model_list;
        }
    }

    /**
     * Возвращает список категорий статей в виде опций для select
     */
    public function getOptions(
        $level = 0,
        $exclude_id = 0,
        $add_root = true
    )
    {

        $ret = [];

        if ($this->getErrors())
        {
            return false;
        }

        if (($add_root) && (0 == $this->id))
        {
            $level++;
            $ret[$this->id] = 'Корневая категория';
        }
        $this->getChildModelList(true);

        foreach ($this->child_model_list as $id_child => $model_gallery)
        {
            if ($id_child != $exclude_id)
            {
                $ret[$id_child] = str_repeat('→', $level) . ' ' . $model_gallery->name.' ('. $id_child . ')';

                foreach ($model_gallery->getOptions($level + 1, $exclude_id) as $key => $value)
                {
                    $ret[$key] = $value;
                }
            }

        }

        return $ret;
    }

    /**
     * Добавляет категорию в БД
     */
    public function addCategory()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_parent = intval($this->id_parent);
        $this->name      = strval($this->name);
        $this->name      = trim($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя категории');

            return false;
        }

        // Получаем список дочерних категорий как ассоциативный массив
        $this->result_sql['addCategory'] = \xtetis\xarticle\models\SqlModel::addCategory(
            $this->id_parent,
            $this->name
        );
        $this->id = intval($this->result_sql['addCategory']['id']);

        return true;
    }

    /**
     * Добавляет категорию в БД
     */
    public function editCategory()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_parent = intval($this->id_parent);
        $this->name      = strval($this->name);
        $this->name      = trim($this->name);

        $this->id = intval($this->id);

        if (!$this->id)
        {
            $this->addError('common', 'Не указан ID');

            return false;
        }

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя категории');

            return false;
        }

        // Получаем список дочерних категорий как ассоциативный массив
        $this->result_sql['editCategory'] = \xtetis\xarticle\models\SqlModel::editCategory(
            $this->id,
            $this->id_parent,
            $this->name
        );

        return true;
    }

    /**
     * Удаляем категорию в БД
     */
    public function deleteCategory()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Получаем список дочерних категорий как ассоциативный массив
        $this->result_sql['deleteArticleCategory'] = \xtetis\xarticle\models\SqlModel::deleteArticleCategory(
            $this->id
        );

        return true;
    }



    /**
     * Возвращает опции "Статей" в виде массива
     * [
     *      ...
     *      id_category => name_category,
     *      ...
     * ]
     *
     */
    public function getOptionsCategory(
        $add_empty = false,
        $level = 1
    )
    {

        $ret       = [];
        $this->id = intval($this->id);

        if ($add_empty)
        {
            $ret[0]   = 'Не указано';
        }

        foreach ($this->getChildModelList() as $id => $model_article_category) 
        {
            $ret[$id]   = ' '.str_repeat('→',$level).' '.$model_article_category->name;
            $ret = $ret+$model_article_category->getOptionsCategory(false,$level+1);
        }

        return $ret;
    }
}
