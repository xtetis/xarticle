<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ArticleInCategoryModel extends \xtetis\xengine\models\Model
{
    /**
     * ID статьи
     */
    public $id_article = 0;

    public $id_category_list = [];


    public $get_article_categories_result = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'id_article',
            'id_category_list',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

    }

    /**
     * Устанавливает категории для
     */
    public function setArticleCategories()
    {
        if ($this->getErrors())
        {
            return false;
        }


        $this->id_article  = intval($this->id_article);

        if (!is_array($this->id_category_list))
        {
            $this->id_category_list = [];
        }
        foreach ($this->id_category_list as &$id_category) {
            $id_category = intval($id_category);
        }

        $this->id_category_list  = array_unique(
            $this->id_category_list
        );


        \xtetis\xarticle\models\SqlModel::setArticleInCategories(
            $this->id_article,
            $this->id_category_list
        );

    }
    

    /**
     * Возвращает список категорий для статьи 
     * ( список ID )
     */
    public function getArticleCategories(
    )
    {
        if ($this->getErrors())
        {
            return false;
        }


        $this->id_article  = intval($this->id_article);
        

        $this->get_article_categories_result = \xtetis\xarticle\models\SqlModel::getArticleCategories($this->id_article);

        if (is_array($this->get_article_categories_result))
        {
            foreach ($this->get_article_categories_result  as $key => $value) {
                $this->id_category_list[]=intval($value['id_category']);
            }
        }

        return true;

    }

}
