<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ArticleAnyfieldComponentModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xarticle_anyfield_component';

    /**
     * Список правил для связанных классов
     */
    public $related_rule_list = [
        'id_category'           => \xtetis\xarticle\models\ArticleCategoryModel::class,
        'id_anyfield_component' => \xtetis\xanyfield\models\ComponentModel::class,
    ];

    /**
     * ID категории статей
     */
    public $id_category = 0;

    /**
     * Включая подкатегории
     */
    public $include_subcategories = 0;

    /**
     * ID компонента Anyfield
     */
    public $id_anyfield_component = 0;

    /**
     * Результат SQL запроса
     */
    public $result_sql = [];

    /**
     * Список моделей self
     */
    public $model_list = [];

    /**
     * Модель категории статьи
     */
    public $model_article_category = false;

    /**
     * Модель компонента Anyfield
     */
    public $model_anyfield_component = false;

    /**
     * Возвращает список моделей
     */
    public function getModelList()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->result_sql['getArticleAnyfieldComponentList'] = \xtetis\xarticle\models\SqlModel::getArticleAnyfieldComponentList();

        if (!$this->result_sql['getArticleAnyfieldComponentList'])
        {
            $this->model_list = [];
        }

        foreach ($this->result_sql['getArticleAnyfieldComponentList'] as $row)
        {
            $model_article_anyfield_component = new self([
                'id' => $row['id'],
            ]);

            $model_article_anyfield_component->getModelById();
            if ($model_article_anyfield_component->getErrors())
            {
                $this->addError('model_article_anyfield_component', $model_article_anyfield_component->getLastErrorMessage());

                return false;
            }

            $this->model_list[$row['id']] = $model_article_anyfield_component;
        }

        return true;

    }

}
