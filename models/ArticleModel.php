<?php

namespace xtetis\xarticle\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ArticleModel extends \xtetis\xengine\models\TableModel
{

    /**
     * @var string
     */
    public $table_name = 'xarticle_article';


    /**
     * Имя статьи
     */
    public $name = '';

    /**
     * Текст статьи
     */
    public $about = '';

    /**
     * Удалена ли статья (в корзине)
     */
    public $deleted = 0;

    /**
     * Список категорий статьи
     */
    public $id_category_list = [];

    /**
     * Предварительный текст страницы
     */
    public $introtext = '';

    /**
     * Дата создания статьи
     */
    public $create_date = '';



    /**
     * Список моделей категорий для указанной статьи
     */
    public $model_article_category_list = [];

    /**
     * Список моделей полей xanyfield 
     * (со значениями для указанной статьи), 
     * которые привязаны к компонентам xanyfield, а те
     * в свою очередь через категорию xarticle к статье
     * 
     * При этом, если для статьи избрано несколько категорий xarticle
     * и к категориям привязаны разные компоненты xanyfield - будет ошибка
     * Это сделано чтобы у статьи был только один набор дополнительных полей
     */
    public $model_xanyfield_field_list = [];

    /**
     * Список моделей компонентов anyfieldComponent, 
     * привязанных к статье через article_category
     */
    public $anyfield_component_model_list = [];

    /**
     * Результат получения данных из SQL запроса
     * 
     * getById - Возвращает статью по ID
     * saveArticle - Сохраняем статью
     */
    public $result_sql = [];

    /**
     * Массив связанных по категории anyfield fieldset
     */
    public $assigned_fieldset_model_list = [];


    /**
     * Сохраняет статью или добавляет новую в зависимости от того, какой
     * id_article был подан
     */
    public function saveArticle()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!is_array($this->id_category_list))
        {
            $this->id_category_list = [];
        }
        foreach ($this->id_category_list as &$id_category)
        {
            $id_category = intval($id_category);
        }

        $this->name      = trim(strval($this->name));
        $this->about     = trim(strval($this->about));
        $this->introtext = trim(strval($this->introtext));

        

        // Сохраняем статью
        $this->result_sql['saveArticle'] = \xtetis\xarticle\models\SqlModel::saveArticle(
            $this->id,
            $this->name,
            $this->about,
            $this->introtext
        );

        

        if ($this->result_sql['saveArticle']['result'] < 0)
        {
            $this->addError('common', $this->result_sql['saveArticle']['result_str']);

            return false;
        }

        $this->id = $this->result_sql['saveArticle']['result'];

        $model_article_in_category = new \xtetis\xarticle\models\ArticleInCategoryModel([
            'id_article'       => $this->id,
            'id_category_list' => $this->id_category_list,
        ]);

        $model_article_in_category->setArticleCategories();
        if ($model_article_in_category->getErrors())
        {
            $this->addError('common', $model_article_in_category->getLastErrorMessage());

            return false;
        }

        return true;
    }

    /**
     * Возвращает информацию о статье по ID
     */
    public function getById(
    )
    {
        /*
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Возвращает статью по ID
        $this->result_sql['getArticleById'] = \xtetis\xarticle\models\SqlModel::getArticleById(
            $this->id
        );

        

        if (!$this->result_sql['getArticleById'])
        {
            $this->addError('getArticleById', __FUNCTION__.': Статьи не существует');

            return false;
        }

        

        $this->name        = $this->result_sql['getArticleById']['name'];
        $this->about       = $this->result_sql['getArticleById']['about'];
        $this->introtext   = $this->result_sql['getArticleById']['introtext'];
        $this->create_date = $this->result_sql['getArticleById']['create_date'];
        $this->deleted = intval($this->result_sql['getArticleById']['deleted']);

        if ($this->id)
        {
            $model_article_in_category = new \xtetis\xarticle\models\ArticleInCategoryModel([
                'id_article' => $this->id,
            ]);

            $model_article_in_category->getArticleCategories();

            $this->id_category_list = $model_article_in_category->id_category_list;
        }

        // Возвращает для статьи список ID дополнительных полей
        $this->getAdditionalFieldList();

        return true;
        */
    }


    /**
     * Возвращает ID компонента для указанной категории статей
     */
    /*
    public static function getAnyfieldComponentByCategory($id_category = 0)
    {
        $id_category = intval($id_category);

        return \xtetis\xarticle\models\SqlModel::getAnyfieldComponentByCategory(
            $id_category
        );
    }
    */

    /**
     * Возвращает информацию по компоненту xanyfield для указанной категории статей
     */
    /*
    public static function getAnyfieldComponentInfoByCategory($id_category = 0)
    {
        
        $id_category = intval($id_category);

        $component_by_category_result = self::getAnyfieldComponentByCategory($id_category);
        if (intval($component_by_category_result['result']) > 0)
        {
            return \xtetis\xanyfield\models\SqlModel::xanyfieldGetComponentInfo(intval($component_by_category_result['result']));
        }
        else
        {
            return false;
        }
    }
    */


    /**
     * Функция возвращает для статьи список ID дополнительных полей
     */
    /*
    public function getAdditionalFieldList()
    {
        
        if ($this->getErrors())
        {
            return false;
        }

        // Генерируем список моделей категорий статей
        foreach ($this->id_category_list as $id_category)
        {
            if (!isset($this->model_xarticle_category_list[$id_category]))
            {
                $article_category_model = new \xtetis\xarticle\models\ArticleCategoryModel([
                    'id' => $id_category,
                ]);

                // Получаем информацию для указанной категории
                $article_category_model->getById();

                if ($article_category_model->getErrors())
                {
                    $this->addError('common', $article_category_model->getLastErrorMessage());

                    return false;
                }

                $this->model_xarticle_category_list[$id_category] = $article_category_model;
                
            }
        }


        // Заполняем список моделей компонентов xanyfield, привязанных к статье через article_category
        foreach ($this->model_xarticle_category_list as $id_article_category => &$model_article_category) 
        {
            $model_article_category->getModelAnyfieldComponentForAllParentsList();

            foreach ($model_article_category->model_anyfield_component_list as $id_anyfield_component => &$model_xanyfield_component) 
            {
                if (!isset($this->model_anyfield_component_list[$id_anyfield_component]))
                {
                    $this->model_anyfield_component_list[$id_anyfield_component] = $model_xanyfield_component;
                }
            }
        }

        if (count($this->model_anyfield_component_list) > 1)
        {
            $this->addError('model_anyfield_component_list', __FUNCTION__.': Статья привязана к нескольким xanyfield_component через категории '.\xtetis\xengine\helpers\JsonHelper::arrayToJson(array_keys($this->model_anyfield_component_list)));

            return false;
        }

        // Устанавливает список дополнительных полей для статьи
        $this->setAdditionalFieldList();

        return true;
        
    }
    */

    /**
     * Устанавливает список дополнительных полей для статьи
     * ХЗ что значит устанавливает
     */
    /*
    public function setAdditionalFieldList()
    {
        
        if ($this->getErrors())
        {
            return false;
        }

        foreach ($this->model_anyfield_component_list as $id_xanyfield_component => &$model_xanyfield_component) 
        {

            $model_xanyfield_component->getIdFieldList();
            
            if ($model_xanyfield_component->getErrors())
            {
                $this->addError('common', $model_xanyfield_component->getLastErrorMessage());

                return false;
            }

            
            foreach ($model_xanyfield_component->model_xanyfield_field_list as &$model_xanyfield_field) 
            {
                if (!isset($this->model_xanyfield_field_list[$model_xanyfield_field->id]))
                {
                    // Присваиваем по какому значению искать значение поля
                    $model_xanyfield_field->value_assign_key = $this->id;


                    $model_xanyfield_field->setFieldInfo();
                    $model_xanyfield_field->getFieldValue();
        
                    if ($model_xanyfield_field->getErrors())
                    {
                        $this->addError('model_xanyfield_field', $model_xanyfield_field->getLastErrorMessage());

                        return false;
                    }

                    $this->model_xanyfield_field_list[$model_xanyfield_field->id] = $model_xanyfield_field;
                }
            }
        }
        
    }
    */

    /**
     * Возвращает массив с моделями категорий статей для указанной статьи
     */
    public function getArticleCategoryModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->model_article_category_list)
        {
            return $this->model_article_category_list;
        }

        $sql = "
            SELECT id_category id
            FROM xarticle_article_category t
            WHERE
                t.id_article = :id_article
        ";
        $params = [
            'id_article'=>$this->id 
        ];

        $this->model_article_category_list = \xtetis\xarticle\models\ArticleCategoryModel::getModelListBySql(
            $sql,
            $params,
            [
                'cache'=>true,
                'cache_tags'=>[
                    'xarticle_article_category'
                ]
            ]
        );

        return $this->model_article_category_list;
    }

    /**
     * Возвращает масств моделей \xtetis\xanyfield\models\ComponentModel 
     * для текущей статьи, с учетом того, в каких категориях находится статья
     */
    /*
    public function getAnyfieldComponentModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Если ранее список компонентов определен - возвращаем его
        if ($this->anyfield_component_model_list)
        {
            return  $this->anyfield_component_model_list;
        }

        foreach($this->getArticleCategoryModelList() as $id_article_category => $model_article_category)
        {
            foreach ($model_article_category->getAnyfieldComponentModelList() as $id_anyfield_component => $model_anyfield_component) {
                if (!isset($this->anyfield_component_model_list[$id_anyfield_component]))
                {
                    $this->anyfield_component_model_list[$id_anyfield_component] = $model_anyfield_component;
                }
            }
        }

        if (count($this->anyfield_component_model_list) > 1)
        {
            throw new \Exception('Количество компонентов anyfield_component привязанных к статье #'.$this->id.' не может быть больше 1');
        }

        return $this->anyfield_component_model_list;
    }
    */


    

    /**
     * Возвращает значение дополнительного поля по имени поля
     * Если значения не существует - возвращает значение по-умолчанию
     */
    public function getAdditionalFieldValue(
        $fieldset_name = '',
        $field_name = '',
        $default_value = ''
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Получаем привязанную модель Fieldset по имени
        $current_fieldset_model = $this->getFieldsetModelByName($fieldset_name);
        
        // Если не найден
        if (!$current_fieldset_model)
        {
            // Возвращаем дефолтное значение
            return $default_value;
        }

        // Перебираем привязанные модели Field
        $current_field_model = $current_fieldset_model->getFieldModelByName($field_name);

        // Указан непривязанный или несуществующий Field
        if (!$current_field_model)
        {
            // Возвращаем дефолтное значение
            return $default_value;
        }

        // Устанавливаем параметры, по которым искать значение
        $current_field_model->id_fieldset      = $current_fieldset_model->id;
        $current_field_model->value_assign_key = $this->id;

        // Получаем значение поля
        if (!$current_field_model->getStoredValue())
        {
            // Возвращаем дефолтное значение
            return $default_value;
        }

        
        return $current_field_model->value;
    }


    /**
     * Возвращает привязанную модель Fieldset по имени или false, если не найдена
     */
    public function getFieldsetModelByName($fieldset_name = '')
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Перебираем привязанные модель Fieldset
        $current_fieldset_model = false;
        foreach ($this->getAssignedFielsetModelList() as $id_fieldset => $model_fieldset) {
            if ($fieldset_name == $model_fieldset->name)
            {
                $current_fieldset_model = $model_fieldset;
                break;
            }
        }

        // Если модель найдена
        if ($current_fieldset_model)
        {
            return $current_fieldset_model;
        }

        return false;

    }


    /**
     * Помечаем статью как удаленную (помещаем в корзину)
     */
    public function setDeleted()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);
        $this->deleted = intval($this->deleted);


        // Сохраняем 
        $this->result_sql['setArticleDeleted'] = \xtetis\xarticle\models\SqlModel::setArticleDeleted(
            $this->id,
            $this->deleted
        );


        return true;
    }


    /**
     * Удаляем статью из БД
     */
    public function deleteArticle()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);
        $this->deleted = intval($this->deleted);


        // Сохраняем 
        $this->result_sql['deleteArticle'] = \xtetis\xarticle\models\SqlModel::deleteArticle(
            $this->id
        );


        return true;
    }


    /**
     * Возвращает массив моделей \xtetis\xanyfield\models\FieldsetModel
     * которые связаны с этой статьей
     */
    public function getAssignedFielsetModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->assigned_fieldset_model_list)
        {
            return $this->assigned_fieldset_model_list;
        }

        $sql = "
        SELECT DISTINCT
            id_fieldset id
        FROM 
            xarticle_category_fieldset cf
        WHERE 
            cf.id_category IN 
            (
                SELECT 
                    id_category
                FROM 
                    xarticle_article_category ac
                WHERE 
                    ac.id_article  = :id_article
            )
        ";
        

        $params = [
            'id_article'=>$this->id
        ];

        $this->assigned_fieldset_model_list = \xtetis\xanyfield\models\FieldsetModel::getModelListBySql(
            $sql,
            $params,
            [
                'cache'=>true,
                'cache_tags'=>[
                    'xarticle_category_fieldset',
                    'xarticle_article_category',
                ]
            ]
        );

        return $this->assigned_fieldset_model_list;
    }

}
