<?php

namespace xarticle;

class Xarticle extends \xtetis\xengine\models\Model
{

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

        // Проверяет параметры
        \ximg\Config::validateParams();

    }

    /**
     * Возвращает список файлов в директории XIMG_PATH
     */
    public function recursiveScanXimg()
    {
        $root = XIMG_PATH;

        return glob("$root/{,*/,*/*/,*/*/*/,*/*/*/*/}*.{jpg,jpeg,png,JPG,JPEG,PNG}", GLOB_BRACE);
    }


    /**
     * Добавляет изображения в XIMG_PATH сведения в БД
     */
    public function initScanXimgDir()
    {
        foreach ($this->recursiveScanXimg() as $key => $filename)
        {
            $img_model = new \ximg\models\ImgModel(
                [
                    'filename' => $filename,
                ]
            );
            $img_model->addImg();
            /*
            if ($img_model->getErrors())
            {
                echo $img_model->getLastErrorMessage()."\n";
            }*/
        }
    }
}
