<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xarticle.js');

    // Урл страницы для валидации формы
    $url_validate_form = \xtetis\xarticle\Module::getModuleUrl('validate_add_article');
?>
<div class="text-left">
    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_form,
    'form_type'    => 'ajax',
    ]);?>
    <h4 class="mb-3 f-w-400 text-center">Новая статья</h4>
    <?=\xtetis\xform\Component::renderField(
        [
            'template'=>'input_text',
            'attributes'=>[
                'label'=> 'Имя (name)',
                'name'=> 'name'
            ],
        ]
    )?>
    <?=\xtetis\xform\Component::renderField(
        [
            'template'=>'textarea',
            'attributes'=>[
                'label'=> 'Текст статьи (about)',
                'name'=> 'about'
            ],
        ]
    )?>
    <button type="submit"
            class="btn btn-block btn-primary mb-4">Добавить</button>
    <?=\xtetis\xform\Component::renderFormEnd();?>
</div>
